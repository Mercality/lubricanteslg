<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('portada');
});

Route::get('/productos', function () {
    return view('productos');
});

Route::get('/contacto', function () {
    return view('contacto');
});

Route::post('/contacto', function () {
	$request = Request::all();
  $data['body'] = $request;
	$mail = Mail::send('emails.test', $data, function ($m) use ($request) {
            $m->from('webadmin@diluga.com.ve', 'Web Admin');
            $m->replyTo($request['email'], $request['name']);

            $m->to('webadmin@diluga.com.ve', 'Web Admin')->subject($request['subject']);
            $m->cc('ramonlv93@gmail.com');
    });
    return redirect('/');
});


Route::get('/filtros', function () {
    return view('filtros');
});

Route::get('/drcare', function () {
    return view('drcare');
});

Route::get('/pdv', function () {
    return view('pdv');
});






Route::get('users', function(Illuminate\Http\Request $request) {
    $users = \App\User::paginate();
    if ($request['salesman']) $users->load('salesman');
    return $users;
});

Route::get('check', function() {
    return \App\Client::whereCode('9696')->first();
});


Route::get('api', ['before' => 'oauth', function(Authorizer $auth) {
 // return the protected resource
 //echo “success authentication”;

    dd(Authorizer::getChecker()->getAccessToken()->expire());
    return Response::json($user);
}]);

Route::group(['prefix' => 'api/v1', /*'middleware' => 'cors'*/], function () {
    Route::resource('users', 'UsersController');



    Route::group(['before' => 'oauth'], function() {
        Route::resource('orders', 'OrdersController');
        Route::resource('clients', 'ClientsController');
        Route::get('auth/user', 'Auth\AuthController@getUser');
        Route::get('auth/apilogout', 'Auth\AuthController@apiLogout');
    });

    Route::resource('products', 'ProductsController');
    Route::resource('salesmen', 'SalesmenController');

    Route::group(['prefix' => 'auth', /*'middleware' => 'auth'*/], function () {
        Route::get('register', 'Auth\AuthController@getRegister');
        Route::post('register', 'Auth\AuthController@postRegister');
        Route::post('login', 'Auth\AuthController@postLogin');
        Route::get('logout', 'Auth\AuthController@getLogout');



        });

    header('Access-Control-Allow-Origin: http://localhost:3000');
    header('Access-Control-Allow-Headers: Content-Type, Authorization');
    header('Access-Control-Allow-Methods: GET, POST, PUT');
    Route::post('oauth/access_token', function() {
     return \Response::json(Authorizer::issueAccessToken());
    });
});

use \GuzzleHttp\Client;

Route::get('/fetchclients', function () {
    setlocale(LC_ALL, 'es_ES');
    $file = fopen(storage_path()."/app/clients.csv", "r+");

    $i = 0;
    $headers = fgetcsv($file, $delimiter = ',');
    $result;
    while(!feof($file))
    {
        $line = fgetcsv($file, $delimiter = ',');

            for ($j = 0; $j <= count($line)-1; $j++) {
                $result[$i][$headers[$j]] = utf8_encode($line[$j]);
            }


        $i++;
    }
    $http = new Client();
    fclose($file);

    foreach($result as $key=>$client) {
        if(isset($client['name'])){
            $verify = \App\Client::whereCode($client['code'])->first();
            if ($verify) {
                $client['cod'] = true;
                $res = $http->request('PUT', url('/').'/api/v1/clients/'.$client['code'], [
                    'json' => $client
                ]);
                echo 'Updated Client: '.$client['code'].'<br>';
            } else {
                $res = $http->request('POST', url('/').'/api/v1/clients', [
                    'json' => $client
                ]);
                echo 'Created Client: '.$client['code'].'<br>';
            }
        }
    }


});

Route::get('/fetchproducts', function () {
    setlocale(LC_ALL, 'es_ES');
    $file = fopen(storage_path()."/app/precios.csv", "r+");

    $i = 0;
    $headers = fgetcsv($file, $delimiter = ',');
	while(!feof($file))
 	{
		$line = fgetcsv($file, $delimiter = ',');

		    for ($j = 0; $j <= count($line)-2; $j++) {
		    	if($j != 7) {
		    		if ($headers[$j] === "price") {
		    			$value = intval(trim(str_replace('.','',$line[$j])));
		    			$result[$i][$headers[$j]] = round($value/100,2);
		    		}

                    else if ($headers[$j] === "stock") {
		    			$value = intval(trim(str_replace('.','',$line[$j])));
		    			$result[$i][$headers[$j]] = $value;
		    		}

                    else if($headers[$j] === "department_id") {
                        $result[$i][$headers[$j]] = intval($line[$j]);
                    }

		    		else {
		    			$result[$i][$headers[$j]] = trim($line[$j]);
		    		}

		    	}

		    }


		$i++;
  	}
    $http = new Client();
    fclose($file);

    foreach($result as $key=>$product) {
        if(isset($product['code'])){
            $verify = \App\Product::whereCode($product['code'])->first();
            if ($verify) {
                $product['cod'] = true;
                $res = $http->request('PUT', url('/').'/api/v1/products/'.$product['code'], [
                    'json' => $product
                ]);
                echo 'Updated Product: '.$product['code'].'<br>';
            } else {
                $res = $http->request('POST', url('/').'/api/v1/products', [
                    'json' => $product
                ]);
                echo 'Created Product: '.$product['code'].'<br>';
            }
        }
    }


});

Route::get('/fetchdepartments', function () {
    setlocale(LC_ALL, 'es_ES');
    $file = fopen(storage_path()."/app/departments.csv", "r+");

    $i = 0;
    $headers = fgetcsv($file, $delimiter = ',');
    $result;
    while(!feof($file))
    {
        $line = fgetcsv($file, $delimiter = ',');

            for ($j = 0; $j <= count($line)-1; $j++) {
                $result[$i][$headers[$j]] = utf8_encode($line[$j]);
            }


        $i++;
    }
    $http = new Client();
    fclose($file);

    foreach($result as $key=>$department) {
        if(isset($department['description'])){
            DB::table('departments')->insert([
                'id' => intval($department['id']),
                'description' => $department['description'],
            ]);
        }
    }


});



Event::listen('illuminate.query', function($query)
{
    Log::info($query);
});
